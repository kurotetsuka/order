## the many disciplines of hacking
hacking is incredibly difficult to define. the most accepted definitions typically describe the qualities of a hacker first, and imply the meaning of hacking as the acts in which a hacker typically enjoys engaging in. this is confusing. hacking is the creation of clever solutions to problems. hacking can also be defined as quality research, design, and development, rather than implementation. due to the looseness of these definitions, every field of technological development falls under the definition of hacking, so long as the development is of good quality.

### main categories
it should be noted that these categories are fairly fuzzy, and are not strict.

#### abstract hacking ( research )
 - completely abstracted away from hardware
 - typically shared as papers

#### soft hacking ( coding )
 - heavily abstracted away from hardware
 - typically shared as code

#### hard hacking ( making )
 - heavily tied to hardware
 - typically shared as designs

#### bio hacking
 - biology

#### chemo hacking
 - chemistry

#### geo hacking
 - geology

#### audio hacking
 - music
 - speaking

#### socio hacking
 - sociology
 - politics
 - activism

#### psycho hacking
 - psychonautics
 - psychology
 - cognitive science
 - philosphy
 - neuroscience

#### visual hacking
 - painting and drawing
 - computer graphics

#### asdf hacking
 - 

#### asdf hacking
 - 

#### asdf hacking
 - 

#### asdf hacking
 - 

#### asdf hacking
 - 

#### ?
 - yoga
 - dance
 - sports
 - martial arts
