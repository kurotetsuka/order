## fundamental rules for a hacker order ( superstructure, overlay government )

 - all hackers are to be treated with respect.
 - code, information, and ideas cannot be owned.
 - code, information, and ideas should not be controlled.
 - good code, information, and ideas must be respected.
 - bad code, information, and ideas may be criticized, deconstructed, ridiculed, etc.
	 - ( ridicule should be discouraged )
	 - bad hackers must not be ridiculed.
		 - ( ridicule of bad hackers would violate rule [ref]. )
	 - the good qualities of bad code, information, and ideas must be respected
 - a hacker must not be forced to give up code, information, or ideas against their will.
	 - ( this rule, in present society, is not absolutely practical. many hackers presently must contribute code, information, and ideas to organizations through a legal process in which they lose attribution and ownership. they often submit themselves to this process in order to gain funds for subsistance. this process breaks rules:\[ ref, ref, ref\] and the practice should be abolished. in order to abolish the practice, all hackers must be able to subsist without contributing. ideally, all hackers should be able to subsist happily. )
 - contribution of code, information, or ideas to the order should be encouraged.
 - contribution of code, information, or ideas to the order can be rewarded.
 - contribution of code, information, or ideas to the order must be attributed to the contribution's author(s), or nobody at all.
	 - hackers may contribute anonymously ( with the understanding that they will not and can not be credited for the contribution, and that the contribution will not be trusted until audited by members of the order ).
		 - ( this may be done with temporary identities. the anonymous hacker wishing to preserve their anonimity must be aware of how speech and other behaviors can reveal identities. for this reason, segregating one's identities may be useful. )
 - all code, information, and ideas contributed to the order must be shared with all members of the order without restriction.
	 - efforts should be made ( and funded ) to make contributions more accessible to the members of the order, especially to those of lesser skill and knowledge. ( this section could be reworded better )
	 - ( present society has many rules regarding the sharing of code, information, and ideas. when contributing to present society, one must explicitly state the terms of usage of the contribution. these terms often contain clauses that violate rules [ref]. in order to avoid conflicts with present society ( as long as the aforementioned rules are in effect ), contributions to the order should be acompanied by terms of usage that do not violate the rules of the order, nor present society. if this cannot be done, the rules of the society must be made compatible before the contribution can be accepted by the order. )
 - members of the order may share code, information, and ideas with others outside the order.
	 - ( members of the order should be extremely careful when sharing code, information, and ideas with others outside the order.
 - 

## practical rules for a hacker order
 - limit sharing and trading with incompatible organizations
	 - ( this rule can be difficult to implement, exceedingly difficult or impossible to enforce. an order's best defense against violations of this rule is trust in its member's affinity with the order's spirit. )
	 - the intensity of this response can be scaled to the scale of the incompatibility.

## possible violations against the spirit of the order
 - exertion power over others
 - hoarding of power
 - exploitation of other's lack of skill or knowledge
 - 

## unenforcable violations against the spirit of the order
 - hoarding of unshared code, information, or ideas
 - 
 - 

## recommendations for handling conflicts between members
 - establish a mediation process
 - 

## recommendations for dealing with unstable elements
 - 

## fundamental conditions for acceptance into the order
 - demonstrated compatibility with the spirit of the order
 - desire to learn
 - desire to contribute
 - desire to teach
 - ( 

## practical conditions for acceptance into the order
 - ability to use cryptographic signing methods
 - ability to use git
 - ability to use dttp

## conditions for expulsion from the order
 - disregard for the rules of the order

## conditions for requested resignation from the order
 - disregard for the spirit of the order
 - unfair exploitation of the order's contribution database for personal gain
 - incompatibility with the spirit of the order
 - disregard for human rights

## guide for establishment of a hacker order
 - select and implement a ranking system
	 - take care not to permit the usage of the ranking system to violate rule 0
	 - let it be understood that those with greater rank are respected more highly, but do not permit disrespect of those of lower rank
	 - always permit full free speech of all members, regardless of rank. 
 - select a promotion model for your ranking system.
 - select a monetary system ( currency ).
	 - encourage members to trade physical possessions and services in exchange for currency.
	 - ideally, the monetary system should be compatible with monetary systems outside the order.
	 - ( ideally, no monetary system would be required, but past history has shown that monetary systems are neccessary for the representation of debts of all sorts ( physical, spiritual, conceptual, etc. ) ). ( this section could be reworded better )
 - select a negotiation process through which hackers may be persuaded to contribute and be rewarded post-contribution.
	 - it may be useful for this negotiation process to accomodate teams, though this may cause unfairness during distribution of rewards within the team.
	 - care should be taken to reward contributions ( negotiated or otherwise ) fairly.
 - select a process through which teams or individuals may accept funding their future work.
 - select a process through which teams or individuals may accept rewards for their past work.
 - implement a web of trust system
 - establish secure communications
 - establish sets of challenges designed to determine the knowledge, ability, and philosophical affinity of hackers.
	 - these challeges could also be used to drive teaching
 - establish databases of learning material.
	 - constantly refine these databases
